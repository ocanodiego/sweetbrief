<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\QuoteService;
use App\Http\Controllers\Controller;

class QuoteController extends Controller
{
    public function getRandomQuote(){

    	return (new QuoteService)->getRandomQuote();
    	
    }

}
