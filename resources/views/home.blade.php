@extends('layouts.app')

@section('content')
<div class="container" id="app">
    <div class="row justify-content-center mt-4">
    	@if(Auth::check())
    	<home></home>
    	@endif
    </div>
</div>
@endsection
